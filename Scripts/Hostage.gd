extends KinematicBody

var speed = 10
var _nav : Navigation
var target_point : Vector3
var moving_direction : Vector3
var calculated_path : PoolVector3Array

func _ready():
	_nav = get_node("/root/Prototype/City/CityFloor/StaticBody/Navigation")
	
	calculated_path = _nav.get_simple_path(get_global_transform().origin, Vector3(0, 1, -34), true)
	moving_direction = transform.origin.direction_to(calculated_path[0])

func _physics_process(delta):
	if calculated_path.size() > 0:
		if transform.origin.distance_to(calculated_path[0]) > 1:
			move_and_slide(moving_direction * speed)
		else:
			calculated_path.remove(0)